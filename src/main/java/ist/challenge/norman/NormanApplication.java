package ist.challenge.norman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NormanApplication {

	public static void main(String[] args) {
		SpringApplication.run(NormanApplication.class, args);
	}

}
