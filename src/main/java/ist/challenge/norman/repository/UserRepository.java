package ist.challenge.norman.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ist.challenge.norman.model.UserAccount;

@Repository
public interface UserRepository extends JpaRepository<UserAccount, Long> {

    UserAccount findByUsername(String username);

     @Query(value = "select * from user_account where password= ?1 ", nativeQuery = true)
     UserAccount findByPassword(String password);

}
