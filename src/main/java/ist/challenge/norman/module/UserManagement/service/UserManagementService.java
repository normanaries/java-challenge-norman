package ist.challenge.norman.module.UserManagement.service;

import ist.challenge.norman.model.UserAccount;
import ist.challenge.norman.module.UserManagement.model.UserManagementRestDto;
import ist.challenge.norman.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserManagementService {

    @Autowired
    UserRepository userRepo;

    public List<UserAccount> findAll(){
        return userRepo.findAll();
    }

    public ResponseEntity<UserManagementRestDto<UserAccount>> update(UserAccount body){
        String username = body.getUsername();
        String password = body.getPassword();

        UserManagementRestDto userManagementRestDto = new UserManagementRestDto();

        if (userRepo.findByUsername(username) != null){
            userManagementRestDto.setMessage("Username telah digunakan");
            userManagementRestDto.setPayload(body);


            return ResponseEntity.status(HttpStatus.CONFLICT).body(userManagementRestDto);
        }


        if (userRepo.findByPassword(password) != null){
            userManagementRestDto.setMessage("Password tidak boleh sama dengan password sebelumnya");
            userManagementRestDto.setPayload(body);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(userManagementRestDto);
        }

        UserAccount userModel = new UserAccount();
        userModel.setUsername(body.getUsername());
        userModel.setPassword(body.getPassword());


        userManagementRestDto.setMessage("Data Username/Password berhasil diubah");
        userManagementRestDto.setPayload(userRepo.save(body));


        return ResponseEntity.status(HttpStatus.CREATED).body(userManagementRestDto);
    }
}
