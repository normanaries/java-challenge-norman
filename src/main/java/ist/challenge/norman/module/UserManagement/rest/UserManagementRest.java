package ist.challenge.norman.module.UserManagement.rest;

import ist.challenge.norman.model.UserAccount;
import ist.challenge.norman.module.UserManagement.model.UserManagementRestDto;
import ist.challenge.norman.module.UserManagement.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserManagementRest {

    @Autowired
    UserManagementService userManagementService;

    @GetMapping("/getAllUsers")
    List<UserAccount> findAll(){
        return userManagementService.findAll();
    }

    @PutMapping("/updateUser")
    ResponseEntity<UserManagementRestDto<UserAccount>> update(@RequestBody UserAccount body){
        return userManagementService.update(body);
    }
}
