package ist.challenge.norman.module.UserManagement.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class UserManagementRestDto <UserManagement> implements Serializable {
    private String message;
    private UserManagement payload;
}
