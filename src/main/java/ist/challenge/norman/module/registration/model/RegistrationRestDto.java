package ist.challenge.norman.module.registration.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationRestDto <Regis> implements Serializable {
    private String message;
    private Regis payload;
}
