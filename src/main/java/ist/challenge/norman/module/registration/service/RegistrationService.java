package ist.challenge.norman.module.registration.service;

import ist.challenge.norman.model.UserAccount;
import ist.challenge.norman.module.registration.model.RegistrationRestDto;
import ist.challenge.norman.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {
    @Autowired
    private UserRepository userRepo;

    public ResponseEntity<RegistrationRestDto<UserAccount>> save(UserAccount body){
        String username = body.getUsername();

        RegistrationRestDto registrationRestDto = new RegistrationRestDto();
        if (userRepo.findByUsername(username) != null){
            registrationRestDto.setMessage("Username telah digunakan");
            registrationRestDto.setPayload(body);

            return ResponseEntity.status(HttpStatus.CONFLICT).body(registrationRestDto);
        }

        UserAccount userAccount = new UserAccount();
        userAccount.setUsername(body.getUsername());
        userAccount.setPassword(body.getPassword());

        registrationRestDto.setMessage("Registrasi Berhasil");
        registrationRestDto.setPayload(userRepo.save(body));


        return ResponseEntity.status(HttpStatus.CREATED).body(registrationRestDto);
    }
    
}
