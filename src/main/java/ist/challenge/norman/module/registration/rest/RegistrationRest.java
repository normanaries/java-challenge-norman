package ist.challenge.norman.module.registration.rest;

import ist.challenge.norman.model.UserAccount;
import ist.challenge.norman.module.registration.model.RegistrationRestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ist.challenge.norman.module.registration.service.RegistrationService;

@RestController
public class RegistrationRest {
    @Autowired
    private RegistrationService registrationService;

    @PostMapping("/register")
    ResponseEntity<RegistrationRestDto<UserAccount>> registration(@RequestBody UserAccount body){
        return registrationService.save(body);
    }
}
