package ist.challenge.norman.module.login.rest;

import ist.challenge.norman.model.UserAccount;
import ist.challenge.norman.module.login.model.LoginRestDto;
import ist.challenge.norman.module.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginRest {

    @Autowired
    LoginService loginService;

    @PostMapping("/login")
    ResponseEntity<LoginRestDto<UserAccount>> login(@RequestBody UserAccount body){
        return loginService.login(body);
    }
}
