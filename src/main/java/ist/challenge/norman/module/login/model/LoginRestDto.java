package ist.challenge.norman.module.login.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginRestDto<string> implements Serializable {
    private string message;

}
