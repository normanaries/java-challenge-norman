package ist.challenge.norman.module.login.service;

import ist.challenge.norman.model.UserAccount;
import ist.challenge.norman.module.login.model.LoginRestDto;
import ist.challenge.norman.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class LoginService {
    @Autowired
    UserRepository userRepo;

    public ResponseEntity<LoginRestDto<UserAccount>> login(UserAccount body) {
        String username = body.getUsername();
        String password = body.getPassword();

        LoginRestDto loginRestDto = new LoginRestDto();

        if (username == null || password == null) {
            loginRestDto.setMessage("username atau password tidak boleh kosong");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(loginRestDto);
        }


        if (userRepo.findByUsername(username) == null || userRepo.findByPassword(password) == null) {
            loginRestDto.setMessage("username atau password salah");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(loginRestDto);
        }

        var getUserId = userRepo.findByPassword(password);
        var getUser = userRepo.findById(getUserId.getId());
        loginRestDto.setMessage("Login berhasil");

        return ResponseEntity.ok(loginRestDto);
    }
}
